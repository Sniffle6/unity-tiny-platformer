
namespace game {

    /** New System */
    export class AttackSystem extends ut.ComponentSystem {

        OnUpdate(): void {
            this.world.forEach([ut.Entity, game.Attack, game.Animations], (entity, attack, animations) => {
                if (attack.Timer == 0) {  
                    attack.ComboStep = false;
                    AnimationSystem.PlayAnimation(this.world, animations, 3);
                } else if (animations.State == 3 && attack.Timer >= 0.5) {
                    AttackSystem.IterateCombo(this.world, entity, animations, attack, 4);
                } else if (animations.State == 4 && attack.Timer >= 1.2) {
                    AttackSystem.IterateCombo(this.world, entity, animations, attack, 5);
                } else if (animations.State == 5 && attack.Timer >= 1.9) {
                    AttackSystem.IterateCombo(this.world, entity, animations, attack, 3);
                }

                if(attack.Timer >= 0.2 && attack.Timer <= 0.5 && !attack.Hit){
                    AttackSystem.ApplyHitImpulse(this.world, entity, new Vector2(0, 5), 5);
                }else if(attack.Timer >= 0.8 && attack.Timer <= 1.0 && !attack.Hit){
                    AttackSystem.ApplyHitImpulse(this.world, entity, new Vector2(0, -7), 10);
                }else if(attack.Timer >= 1.3 && attack.Timer <= 1.8 && !attack.Hit){
                    let scale = this.world.getComponentData(entity, ut.Core2D.TransformLocalScale);
                    AttackSystem.ApplyHitImpulse(this.world, entity, new Vector2((7*scale.scale.x), 0), 15);
                }

                attack.Timer += this.scheduler.deltaTime();
            });


        }

        static ApplyHitImpulse(world:ut.World, entity: ut.Entity, impulseForce:Vector2, damage:number){
           let atk = world.getComponentData(entity, game.Attack);
           
            world.forEach([ut.Entity, game.Destructible, ut.Core2D.TransformLocalPosition], [ut.Subtractive(game.Dying)], (destructibleEnt, destTag, dtransform)=>{
                let transformScale = world.getComponentData(entity, ut.Core2D.TransformLocalScale);
                let transform = world.getComponentData(entity, ut.Core2D.TransformLocalPosition);

                if(transform.position.distanceTo(dtransform.position) < 2){
                    if(GameService.IsFacingEntity(transform, dtransform, transformScale)){
                        atk.Hit = true;
                        world.setComponentData(entity, atk);
                        AnimationSystem.PlayOneShot(world, destructibleEnt, 0.61, 6);
                        HealthSystem.AdjustHealth(world, destructibleEnt, damage);
                        let impulse = new ut.Physics2D.AddImpulse2D;
                        impulse.impulse = impulseForce;
                        world.addComponentData(destructibleEnt, impulse);
                    }
                }

            });
        }

        static IterateCombo(world: ut.World, entity: ut.Entity, animations: game.Animations, attack: game.Attack, state: number): void {
            if (attack.ComboStep) {
                if (animations.State == 5)
                    attack.Timer = 0;
                attack.Hit = false;
                attack.ComboStep = false;
                AnimationSystem.PlayAnimation(world, animations, state);
                console.log("iterating");
            }else{
                console.log("removing");
                world.removeComponent(entity, game.Attack);
            }
        }
    }
}


namespace game {

    /** New System */
    export class DeathSystem extends ut.ComponentSystem {
        
        OnUpdate():void {
            this.world.forEach([ut.Entity, game.Dying], (entity, dying) => {
                if(dying.TimePlayed == 0){
                    if(this.world.hasComponent(entity, game.Animations)){
                        AnimationSystem.PlayOneShot(this.world, entity, 1, 7);
                    }
                }
                if(dying.TimePlayed >= dying.Length){
                    GameService.setEntityEnabled(this.world, entity, false);
                }
                dying.TimePlayed += this.scheduler.deltaTime();
            });
        }
    }
}

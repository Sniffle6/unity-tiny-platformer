
namespace game {

    /** New System */
    export class GameManagerService extends ut.ComponentSystem {

        OnUpdate(): void {
            var config = this.world.getConfigData(game.GameConfig);
            switch (config.State) {
                case game.GameState.Initialize:
                    console.log("Starting Init");
                    game.GameService.Initialize(this.world);
                    break;
                case game.GameState.Play:
                    if (ut.Runtime.Input.getKeyDown(ut.Core2D.KeyCode.U)) {
                        let ent = ut.EntityGroup.instantiate(this.world, GameService.name_enemy)[0];
                        let trans = this.world.getComponentData(ent, ut.Core2D.TransformLocalPosition);
                        trans.position = new Vector3(GameService.getRandom(-14, 14), GameService.getRandom(-2, 5));
                        this.world.setComponentData(ent, trans);
                    }
                    break;
                case game.GameState.GameOver:
                    break;
            }
        }
    }
}

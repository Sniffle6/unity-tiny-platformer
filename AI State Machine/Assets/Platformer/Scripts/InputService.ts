
namespace game {

    /** New System */
    export class InputService {
        static Move(input:game.Input, movement:game.Movement, direction:Vector2):void{
            input.Axis = direction;
            movement.Direction = input.Axis;
        }
        static Jump(movement:game.Movement){
            movement.ShouldJump = true;
        }
        static Attack(world:ut.World, entity:ut.Entity){
            if(world.hasComponent(entity, Attack)){
                let atk = world.getComponentData(entity, game.Attack);
                atk.ComboStep = true;
                world.setComponentData(entity, atk);
                return;
            }
            let attack = new game.Attack;
            world.addComponentData(entity, attack);
        }
    }
}

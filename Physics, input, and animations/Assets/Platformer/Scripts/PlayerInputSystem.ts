
namespace game {
@ut.executeBefore(ut.Shared.InputFence)
    /** New System */
    export class PlayerInputSystem extends ut.ComponentSystem {
        
        OnUpdate():void {
            this.world.forEach([game.PlayerInput, game.Movement], (input, movement) => {
                if(ut.Runtime.Input.getKey(ut.Core2D.KeyCode.A)){
                    input.Axis = new Vector2(-1, input.Axis.y);
                }
                else if(ut.Runtime.Input.getKey(ut.Core2D.KeyCode.D)){
                    input.Axis = new Vector2(1, input.Axis.y);
                }else{
                    input.Axis = new Vector2(0, input.Axis.y);
                }
                movement.Direction = input.Axis;
                movement.Jump = ut.Runtime.Input.getKeyDown(ut.Core2D.KeyCode.Space);
            });
        }
    }
}


namespace game {

    /** New System */
    export class GameService {
       

        static setEntityEnabled(world: ut.World, entity: ut.Entity, enabled: boolean){
            let hasDisabledComponet = world.hasComponent(entity, ut.Disabled);
            if(enabled && hasDisabledComponet){
                world.removeComponent(entity, ut.Disabled);
            }
            else if( !enabled && !hasDisabledComponet){
                world.addComponent(entity, ut.Disabled);
            }
        }

    }
}

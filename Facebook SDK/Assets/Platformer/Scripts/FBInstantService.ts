namespace game {
 
    export class FBInstantService {
 
        protected static Instance: FBInstantService = null;
        protected hasInstant: boolean = false;
 
        public static getInstance(): FBInstantService {
            if (FBInstantService.Instance === null) {
                FBInstantService.Instance = new FBInstantService();
            }
            return FBInstantService.Instance;
        }
 
        public async initialize() {
            try {          
                FBInstant; // this will error out if it's not available
                this.hasInstant = true;
                // let's do our async and goto main menu.
                await FBInstant.initializeAsync();
                FBInstant.setLoadingProgress(100);
                console.log("Starting game async...");
                return await FBInstant.startGameAsync();
            } catch (e) {
                // We don't have this, so let's  just goto the main menu state
                this.hasInstant = false;
                return;
            }
        }
 
        public get isAvailable(): boolean {
            return this.hasInstant;
        }
    }
 
 
    export enum ContextFilter {
        NEW_CONTEXT_ONLY = "NEW_CONTEXT_ONLY",
        INCLUDE_EXISTING_CHALLENGES = "INCLUDE_EXISTING_CHALLENGES",
        NEW_PLAYERS_ONLY = "NEW_PLAYERS_ONLY",
    }
}
 

namespace game {

    /** New System */
    export class AiInputSystem extends ut.ComponentSystem {

        OnUpdate(): void {
            this.world.forEach([ut.Entity, game.Input, game.Movement, game.Ai, ut.Core2D.TransformLocalPosition],
                (entity, input, movement, aiTag, transform) => {
                    AiInputSystem.OnStateUpdate(this.world, aiTag, entity, transform)

                });
        }

        static ChangeState(world: ut.World, entity: ut.Entity, aiTag: game.Ai, state: game.AiState) {
            if (aiTag.State == state)
                return;
            this.OnStateLeave(world, entity, state);
            aiTag.Previous = aiTag.State;
            aiTag.State = state;
            this.OnStateEnter(world, entity, state);
        }
        static OnStateLeave(world: ut.World, entity: ut.Entity, state: game.AiState) {
            switch (state) {
                case game.AiState.Patrolling:
                    if (world.hasComponent(entity, game.Patrol))
                        world.removeComponent(entity, game.Patrol);
                    break;
            }
        }
        static OnStateEnter(world: ut.World, entity: ut.Entity, state: game.AiState) {
            switch (state) {
                case game.AiState.Patrolling:
                    let patrol = new game.Patrol;
                    world.addComponentData(entity, patrol);
                    break;
            }
        }
        static OnStateUpdate(world: ut.World, aiTag: game.Ai, entity: ut.Entity, transform: ut.Core2D.TransformLocalPosition) {
            this.ShouldAttack(world, entity, transform, aiTag);
            this.ShouldPatrol(world, entity, transform, aiTag);
        }
        static ShouldPatrol(world: ut.World, entity: ut.Entity, transform: ut.Core2D.TransformLocalPosition, aiTag: game.Ai) {
            if (aiTag.State == game.AiState.Attacking)
                return;
            if (GameService.CanWalkLeft(world, transform) || GameService.CanWalkRight(world, transform))
                this.ChangeState(world, entity, aiTag, game.AiState.Patrolling);
        }
        static ShouldAttack(world: ut.World, entity: ut.Entity, transform: ut.Core2D.TransformLocalPosition, aiTag: game.Ai) {
            let stopAtk: boolean = true;
            world.forEach([game.Player, ut.Core2D.TransformLocalPosition], (playerTag, playerTransform) => {
                let transformScale = world.getComponentData(entity, ut.Core2D.TransformLocalScale);
                if (transform.position.distanceTo(playerTransform.position) < 2) {
                    if (GameService.IsFacingEntity(transform, playerTransform, transformScale)) {
                        InputService.Attack(world, entity);
                        this.ChangeState(world, entity, aiTag, game.AiState.Attacking);
                        stopAtk = false;
                    }
                }
            });
            if (stopAtk && aiTag.State == game.AiState.Attacking) {
                this.ChangeState(world, entity, aiTag, game.AiState.Idle);
            }
        }
    }
}

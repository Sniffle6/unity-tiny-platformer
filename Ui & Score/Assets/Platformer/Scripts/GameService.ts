
namespace game {

    /** New System */
    export class GameService {

        private static Camera: ut.Entity;
        private static name_level: string = 'game.LevelGroup';
        private static name_player: string = 'game.PlayerGroup';
        public static name_enemy: string = 'game.EnemyGroup';
        public static ui_Game: string = 'game.GameUI';
        static Initialize(world: ut.World) {
            ut.EntityGroup.instantiate(world, this.name_level);
            ut.EntityGroup.instantiate(world, this.name_player);
            ut.EntityGroup.instantiate(world, this.name_enemy);
            ut.EntityGroup.instantiate(world, this.ui_Game);

            let gameConfig = world.getConfigData(game.GameConfig);
            gameConfig.State = game.GameState.Play;
            world.setConfigData(gameConfig);
        }
        static CanWalkLeft(world: ut.World, transform: ut.Core2D.TransformLocalPosition) {
            let _hitResultGround = ut.HitBox2D.HitBox2DService.hitTest(world, transform.position.add(new Vector3(-1, -0.5, 0)), this.GetCamera(world));
            let _hitResultWall = ut.HitBox2D.HitBox2DService.hitTest(world, transform.position.add(new Vector3(-1, 1, 0)), this.GetCamera(world));
            if (!_hitResultGround.entityHit.isNone() && _hitResultWall.entityHit.isNone())
                return true;
            else
                return false;
        }
        static CanWalkRight(world: ut.World, transform: ut.Core2D.TransformLocalPosition) {
            let _hitResultGround = ut.HitBox2D.HitBox2DService.hitTest(world, transform.position.add(new Vector3(1, -0.5, 0)), this.GetCamera(world));
            let _hitResultWall = ut.HitBox2D.HitBox2DService.hitTest(world, transform.position.add(new Vector3(1, 1, 0)), this.GetCamera(world));
            if (!_hitResultGround.entityHit.isNone() && _hitResultWall.entityHit.isNone())
                return true;
            else
                return false;
        }

        static setEntityEnabled(world: ut.World, entity: ut.Entity, enabled: boolean) {
            let hasDisabledComponet = world.hasComponent(entity, ut.Disabled);
            if (enabled && hasDisabledComponet) {
                world.removeComponent(entity, ut.Disabled);
            }
            else if (!enabled && !hasDisabledComponet) {
                if (world.hasComponent(entity, game.Animations)) {
                    let anim = world.getComponentData(entity, game.Animations);
                    AnimationSystem.DisableAnimations(world, anim);
                }
                world.addComponent(entity, ut.Disabled);
            }
        }

        static GetCamera(world: ut.World): ut.Entity {
            if (this.Camera == null) {
                let cameraEntity: ut.Entity;
                world.forEach([ut.Core2D.Camera2D, ut.Entity], (camera, camEntity) => {
                    cameraEntity = new ut.Entity(camEntity.index, camEntity.version);
                });
                this.Camera = cameraEntity;
                return cameraEntity;
            } else {
                return this.Camera;
            }
        }
        static IsFacingEntity(transform1: ut.Core2D.TransformLocalPosition, transform2: ut.Core2D.TransformLocalPosition, transform1Scale: ut.Core2D.TransformLocalScale) {
            return transform1.position.x > transform2.position.x && transform1Scale.scale.x == -1 || transform1.position.x < transform2.position.x && transform1Scale.scale.x == 1;
        }
        static getRandom(min: number, max: number): number {
            return Math.random() * (max - min + 1) + min;
        }

    }
}

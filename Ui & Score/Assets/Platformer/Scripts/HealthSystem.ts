
namespace game {

    /** New System */
    export class HealthSystem extends ut.ComponentSystem {
        
        OnUpdate():void {

        }
        static AdjustHealth(world:ut.World, entity: ut.Entity, value:number):boolean{
            if(world.hasComponent(entity, game.Health)){
                let health = world.getComponentData(entity, game.Health);
                health.CurrentHp -= value;
                world.setComponentData(entity, health);
                console.log(entity, health);
                if(health.CurrentHp <= 0){
                    this.Die(world, entity);
                    return true;
                }
            }
            else{
                this.Die(world, entity);
                return true;
            }
            return false;
        }
        static Die(world:ut.World, entity:ut.Entity){
            if(!world.hasComponent(entity, game.Dying)){
                let dying = new game.Dying;
                dying.Length = 1;
                world.addComponentData(entity, dying);
            }
        }
    }
}

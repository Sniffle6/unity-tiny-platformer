
namespace game {
@ut.executeBefore(ut.Shared.InputFence)
    /** New System */
    export class PlayerInputSystem extends ut.ComponentSystem {
        
        OnUpdate():void {
            this.world.forEach([ut.Entity, game.Input, game.Movement, game.Player], (entity, input, movement, playerTag) => {
                if(ut.Runtime.Input.getKey(ut.Core2D.KeyCode.A)){
                    InputService.Move(input, movement, new Vector2(-1, input.Axis.y));
                }
                else if(ut.Runtime.Input.getKey(ut.Core2D.KeyCode.D)){
                    InputService.Move(input, movement, new Vector2(1, input.Axis.y));
                }else{
                    InputService.Move(input, movement, new Vector2(0, input.Axis.y));
                }

                if(ut.Runtime.Input.getKeyDown(ut.Core2D.KeyCode.Space)){
                    InputService.Jump(movement);
                }


                if(ut.Runtime.Input.getKeyDown(ut.Core2D.KeyCode.J)){
                    InputService.Attack(this.world, entity);
                }


            });
        }
    }
}

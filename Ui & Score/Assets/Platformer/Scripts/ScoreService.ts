
namespace game {

    /** New System */
    export class ScoreService {
        static ScoreEntity: ut.Entity;

        static getScore(world: ut.World): game.Score {
            if (!world.exists(this.ScoreEntity)) {
                this.ScoreEntity = world.getEntityByName("Score");
                if (!world.exists(this.ScoreEntity)) {
                    this.ScoreEntity = null;
                    return null;
                }
            }
            return world.getComponentData(this.ScoreEntity, game.Score);
        }
        static AdjustScore(world:ut.World, value:number){
            let score = this.getScore(world);
            score.Value += value;
            world.setComponentData(this.ScoreEntity, score);
        }
    }
}
